package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class HasilActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_hasil;
    Button btn_back;

    public static String DATA2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hasil);

        tv_hasil = findViewById(R.id.tv_hasil);
        btn_back = findViewById(R.id.btn_back);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String dataToCalculate = bundle.getString(MainActivity.DATA);
        String finalResult = getResult(dataToCalculate);
        if(!finalResult.equals("Err") && !dataToCalculate.equals("")) {
            tv_hasil.setText(dataToCalculate + "=" + finalResult);
        }
        btn_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    String getResult(String data){
        try{
            Context context = Context.enter();
            context.setOptimizationLevel(-1);
            Scriptable scriptable = context.initStandardObjects();
            String finalResult = context.evaluateString(scriptable,data,"Javascript",1,null).toString();
            if(finalResult.endsWith(".0")){
                finalResult = finalResult.replace(".0","");
            }
            return finalResult;
        } catch (Exception e) {
            return "Err";
        }
    }
}