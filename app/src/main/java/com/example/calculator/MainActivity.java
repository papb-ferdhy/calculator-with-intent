package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Script;
import org.mozilla.javascript.Scriptable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView solusiTv;
    MaterialButton btnHapus, btnKurungBuka, btnKurungTutup, btnBagi, btnKali, btnKurang, btnTambah, btnHasil, btnKoma, btnAC;
    MaterialButton btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0;

    public static final String DATA = "hasil";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        solusiTv = findViewById(R.id.tv_solution);

        assignId(btnHapus, R.id.button_backspace);
        assignId(btnKurungBuka, R.id.button_kurung_buka);
        assignId(btnKurungTutup, R.id.button_kurung_tutup);
        assignId(btnBagi, R.id.button_bagi);
        assignId(btnKali, R.id.button_kali);
        assignId(btnKurang, R.id.button_kurang);
        assignId(btnTambah, R.id.button_tambah);
        assignId(btnHasil, R.id.button_hasil);
        assignId(btnKoma, R.id.button_koma);
        assignId(btnAC, R.id.button_ac);
        assignId(btn1, R.id.button_1);
        assignId(btn2, R.id.button_2);
        assignId(btn3, R.id.button_3);
        assignId(btn4, R.id.button_4);
        assignId(btn5, R.id.button_5);
        assignId(btn6, R.id.button_6);
        assignId(btn7, R.id.button_7);
        assignId(btn8, R.id.button_8);
        assignId(btn9, R.id.button_9);
        assignId(btn0, R.id.button_0);
    }

    void assignId(MaterialButton btn, int id) {
        btn = findViewById(id);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        MaterialButton button = (MaterialButton) view;
        String buttonText = button.getText().toString();
        String dataToCalculate = solusiTv.getText().toString();

        switch (buttonText) {
            case "÷" : buttonText = "/"; break;
            case "," : buttonText = "."; break;
        }

        if(buttonText.equals("AC")){
            solusiTv.setText("0");
            return;
        }
        if(buttonText.equals("=")){
            Intent intent = new Intent(this, HasilActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(DATA, solusiTv.getText().toString());
            intent.putExtras(bundle);
            startActivity(intent);
            return;
        }
        if(buttonText.equals("c")){
            if(dataToCalculate.length() > 1) {
                dataToCalculate = dataToCalculate.substring(0,dataToCalculate.length()-1);
            } else if (solusiTv.length() == 1) {
                dataToCalculate = "0";
            }
        } else {
            if(dataToCalculate.equals("0")) {
                if(!buttonText.equals(".")) dataToCalculate = "";
            } else {
                solusiTv.setText(dataToCalculate);
            }
            dataToCalculate+=buttonText;
        }

        solusiTv.setText(dataToCalculate);

    }

}